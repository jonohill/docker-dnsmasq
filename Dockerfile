FROM alpine

ENV CONFIG_PATH=/config/dnsmasq.conf

RUN apk add --no-cache dnsmasq

ENTRYPOINT [ "dnsmasq" ]
CMD [ "--conf-file=${CONFIG_PATH}", "--no-daemon" ]
