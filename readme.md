# dnsmasq

A simple alpine-based image with dnsmasq.
To use, mount a dnsmasq config file to `/config/dnsmasq.conf`. If you need to you can change this path by setting the `CONFIG_PATH` variable.

## Tags

The image is rebuilt daily to pick up any alpine/dnsmasq updates. Images are built for amd64/arm7/arm64, tagged with the dnsmasq version, and uploaded with the appropriate architecture manifest.
